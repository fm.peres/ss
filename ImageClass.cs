﻿using System;
using System.Collections.Generic;
using System.Text;
using Emgu.CV.Structure;
using Emgu.CV;

namespace SS_OpenCV
{
    class ImageClass
    {

        /// <summary>
        /// Image Negative using EmguCV library
        /// Slower method
        /// </summary>
        /// <param name="img">Image</param>
        public unsafe static void Negative(Image<Bgr, byte> img)
        {

            int x, y;
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            byte blue, green, red;
            int nChannels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            
            for (y = 0; y < height; y++)
            {
                for (x = 0; x < width; x++)
                {
                    blue = data_ptr[0];
                    green = data_ptr[1];
                    red = data_ptr[2];

                    // acesso directo : mais lento 
                    data_ptr[0] = (byte) ( 255 - (int)data_ptr[0]);
                    data_ptr[1] = (byte) ( 255 - (int)data_ptr[1]);
                    data_ptr[2] = (byte) ( 255 - (int)data_ptr[2]);
                    data_ptr += nChannels;

                }
                data_ptr += padding;
            }
        }

        /// <summary>
        /// Convert to gray
        /// Direct access to memory - faster method
        /// </summary>
        /// <param name="img">image</param>
        public static void ConvertToGray(Image<Bgr, byte> img)
        {
            unsafe
            {
                // direct access to the image memory(sequencial)
                // direcion top left -> bottom right

                MIplImage m = img.MIplImage;
                byte* dataPtr = (byte*)m.imageData.ToPointer(); // Pointer to the image
                byte blue, green, red, gray;

                int width = img.Width;
                int height = img.Height;
                int nChan = m.nChannels; // number of channels - 3
                int padding = m.widthStep - m.nChannels * m.width; // alinhament bytes (padding)
                int x, y;

                if (nChan == 3) // image in RGB
                {
                    for (y = 0; y < height; y++)
                    {
                        for (x = 0; x < width; x++)
                        {
                            //retrive 3 colour components
                            blue = (dataPtr + x * nChan + y * m.widthStep)[0];
                            green = (dataPtr + x * nChan + y * m.widthStep)[1];
                            red = (dataPtr + x * nChan + y * m.widthStep)[2];

                            // convert to gray
                            gray = (byte)Math.Round(((int)blue + green + red) / 3.0);

                            // store in the image
                            (dataPtr + x * nChan + y * m.widthStep)[0] = gray;
                            (dataPtr + x * nChan + y * m.widthStep)[1] = gray;
                            (dataPtr + x * nChan + y * m.widthStep)[2] = gray;

                            // advance the pointer to the next pixel
                            //dataPtr += nChan;

                        }

                        //at the end of the line advance the pointer by the aligment bytes (padding)
                        //dataPtr += padding;
                    }
                }
            }
        }

        public static void ConvertToRed(Image<Bgr, byte> img)
        {
            unsafe
            {
                // direct access to the image memory(sequencial)
                // direcion top left -> bottom right

                MIplImage m = img.MIplImage;
                byte* dataPtr = (byte*)m.imageData.ToPointer(); // Pointer to the image
                byte blue, green, red;

                int width = img.Width;
                int height = img.Height;
                int nChan = m.nChannels; // number of channels - 3
                int padding = m.widthStep - m.nChannels * m.width; // alinhament bytes (padding)
                int x, y;

                if (nChan == 3) // image in RGB
                {
                    for (y = 0; y < height; y++)
                    {
                        for (x = 0; x < width; x++)
                        {
                            //retrive 3 colour components
                            blue = dataPtr[0];
                            green = dataPtr[1];
                            red = dataPtr[2];


                            // store in the image
                            dataPtr[0] = red;
                            dataPtr[1] = red;

                            // advance the pointer to the next pixel
                            dataPtr += nChan;
                        }

                        //at the end of the line advance the pointer by the aligment bytes (padding)
                        dataPtr += padding;
                    }
                }
            }
        }

        public static void ConvertToBlue(Image<Bgr, byte> img)
        {
            unsafe
            {
                // direct access to the image memory(sequencial)
                // direcion top left -> bottom right

                MIplImage m = img.MIplImage;
                byte* dataPtr = (byte*)m.imageData.ToPointer(); // Pointer to the image
                byte blue, green, red;

                int width = img.Width;
                int height = img.Height;
                int nChan = m.nChannels; // number of channels - 3
                int padding = m.widthStep - m.nChannels * m.width; // alinhament bytes (padding)
                int x, y;

                if (nChan == 3) // image in RGB
                {
                    for (y = 0; y < height; y++)
                    {
                        for (x = 0; x < width; x++)
                        {
                            //retrive 3 colour components
                            blue = dataPtr[0];
                            green = dataPtr[1];
                            red = dataPtr[2];


                            // store in the image
                            dataPtr[1] = blue;
                            dataPtr[2] = blue;

                            // advance the pointer to the next pixel
                            dataPtr += nChan;
                        }

                        //at the end of the line advance the pointer by the aligment bytes (padding)
                        dataPtr += padding;
                    }
                }
            }
        }

        public static void ConvertToGreen(Image<Bgr, byte> img)
        {
            unsafe
            {
                // direct access to the image memory(sequencial)
                // direcion top left -> bottom right

                MIplImage m = img.MIplImage;
                byte* dataPtr = (byte*)m.imageData.ToPointer(); // Pointer to the image
                byte blue, green, red;

                int width = img.Width;
                int height = img.Height;
                int nChan = m.nChannels; // number of channels - 3
                int padding = m.widthStep - m.nChannels * m.width; // alinhament bytes (padding)
                int x, y;

                if (nChan == 3) // image in RGB
                {
                    for (y = 0; y < height; y++)
                    {
                        for (x = 0; x < width; x++)
                        {
                            //retrive 3 colour components
                            blue = dataPtr[0];
                            green = dataPtr[1];
                            red = dataPtr[2];


                            // store in the image
                            dataPtr[0] = green;
                            dataPtr[2] = green;

                            // advance the pointer to the next pixel
                            dataPtr += nChan;
                        }

                        //at the end of the line advance the pointer by the aligment bytes (padding)
                        dataPtr += padding;
                    }
                }
            }
        }

        public unsafe static void Contrast_Brightness(Image<Bgr, byte> img, double contrast, int brightness)
        {

            int x, y;
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            byte blue, green, red;
            int nChannels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;


            for (y = 0; y < height; y++)
            {
                for (x = 0; x < width; x++)
                {
                    blue = data_ptr[0];
                    green = data_ptr[1];
                    red = data_ptr[2];

                    int blue_res = (int)((contrast * (double)blue) + (double)brightness);
                    int green_res = (int)((contrast * (double)green) + (double)brightness);
                    int red_res = (int)((contrast * (double)red) + (double)brightness);

                    if (blue_res > 255) blue_res = 255;
                    if (green_res > 255) green_res = 255;
                    if (red_res > 255) red_res = 255;

                    // acesso directo : mais lento 
                    data_ptr[0] = (byte)blue_res;
                    data_ptr[1] = (byte)green_res;
                    data_ptr[2] = (byte)red_res;
                    data_ptr += nChannels;

                }
                data_ptr += padding;
            }
        }

        // Translation helper function
        private unsafe static void TranslationXPositiveYPositive(Image<Bgr, byte> img, int offset_x, int offset_y) {
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            int n_channels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int width_step = mipl.widthStep;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            // Create a clone of image to pixel overwriting
            Image<Bgr, byte> image_clone = img.Clone();
            byte* data_ptr_clone = (byte*)image_clone.MIplImage.imageData.ToPointer();

            for (int y = offset_y; y < height; y++) {
                for (int x = offset_x; x < width; x++) {
                    byte* new_byte = (byte*)(data_ptr_clone + (x - offset_x) * n_channels +  (y - offset_y) * width_step);
                    byte* byte_to_change = (byte*)(data_ptr + x * n_channels + y * width_step);

                    byte_to_change[0] = new_byte[0];
                    byte_to_change[1] = new_byte[1];
                    byte_to_change[2] = new_byte[2];
                }
            }
        }

        // Translation helper function
        private unsafe static void TranslationXPositiveYNegative(Image<Bgr, byte> img, int offset_x, int offset_y) {
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            int n_channels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int width_step = mipl.widthStep;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            // Create a clone of image to pixel overwriting
            Image<Bgr, byte> image_clone = img.Clone();
            byte* data_ptr_clone = (byte*)image_clone.MIplImage.imageData.ToPointer();

            for (int y = 0; y < height + offset_y; y++) {
                for (int x = offset_x; x < width; x++) {
                    byte* new_byte = (byte*)(data_ptr_clone + (x - offset_x) * n_channels +  (y - offset_y) * width_step);
                    byte* byte_to_change = (byte*)(data_ptr + x * n_channels + y * width_step);

                    byte_to_change[0] = new_byte[0];
                    byte_to_change[1] = new_byte[1];
                    byte_to_change[2] = new_byte[2];
                }
            }
        }

        // Translation helper function
        private unsafe static void TranslationXNegativeYPositive(Image<Bgr, byte> img, int offset_x, int offset_y) {
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            int n_channels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int width_step = mipl.widthStep;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            // Create a clone of image to pixel overwriting
            Image<Bgr, byte> image_clone = img.Clone();
            byte* data_ptr_clone = (byte*)image_clone.MIplImage.imageData.ToPointer();
            
            for (int y = offset_y; y < height; y++) {
                for (int x = 0; x < width + offset_x; x++) {
                    byte* new_byte = (byte*)(data_ptr_clone + (x - offset_x) * n_channels +  (y - offset_y) * width_step);
                    byte* byte_to_change = (byte*)(data_ptr + x * n_channels + y * width_step);

                    byte_to_change[0] = new_byte[0];
                    byte_to_change[1] = new_byte[1];
                    byte_to_change[2] = new_byte[2];
                }
            }
        }

        // Translation helper function
        private unsafe static void TranslationXNegativeYNegative(Image<Bgr, byte> img, int offset_x, int offset_y) {
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            int n_channels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int width_step = mipl.widthStep;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            // Create a clone of image to pixel overwriting
            Image<Bgr, byte> image_clone = img.Clone();
            byte* data_ptr_clone = (byte*)image_clone.MIplImage.imageData.ToPointer();

            for (int y = 0; y < height + offset_y; y++) {
                for (int x = 0; x < width + offset_x; x++) {
                    byte* new_byte = (byte*)(data_ptr_clone + (x - offset_x) * n_channels +  (y - offset_y) * width_step);
                    byte* byte_to_change = (byte*)(data_ptr + x * n_channels + y * width_step);

                    byte_to_change[0] = new_byte[0];
                    byte_to_change[1] = new_byte[1];
                    byte_to_change[2] = new_byte[2];
                }
            }
        }

        public unsafe static void Translation(Image<Bgr, byte> img, int trans_x, int trans_y)
        {
            if (trans_x >= 0 && trans_y >= 0) {
                TranslationXPositiveYPositive(img, trans_x, trans_y);
            }
            else if (trans_x >= 0 && trans_y < 0) {
                TranslationXPositiveYNegative(img, trans_x, trans_y);
            }
            else if (trans_x < 0 && trans_y >= 0) {
                TranslationXNegativeYPositive(img, trans_x, trans_y);
            }
            else {
                TranslationXNegativeYNegative(img, trans_x, trans_y);
            }
        }

        public unsafe static void Rotation(Image<Bgr, byte> img, double degrees)
        {
            int x, y;
            MIplImage mipl = img.MIplImage;
            byte* data_ptr = (byte*)mipl.imageData.ToPointer();
            int nChannels = mipl.nChannels;
            int height = img.Height;
            int width = img.Width;
            int padding = mipl.widthStep - mipl.nChannels * mipl.width;

            double rad_degrees = degrees * Math.PI / 180;

            double rot_cos = Math.Cos(rad_degrees);
            double rot_sin = Math.Sin(rad_degrees);

            for (y = 0; y < height; y++)
            {
                for (x = 0; x < width; x++)
                {
                    double xx = x * rot_cos + y * rot_sin;
                    double yy = -(x * rot_sin) + y * rot_cos;
                    double x_new = (xx - width/2) * rot_cos - (height/2 - yy) * rot_sin + width/2;
                    double y_new = height/2 - (xx - width/2) * rot_sin - (height/2 - yy) * rot_cos;
                    int t = (int)( nChannels * (x_new + y_new) + (y_new * padding) );
                    if (t>0)
                    {
                        (data_ptr + x * nChannels + y * mipl.widthStep)[0] = data_ptr[t];
                        (data_ptr + x * nChannels + y * mipl.widthStep)[1] = data_ptr[t + 1];
                        (data_ptr + x * nChannels + y * mipl.widthStep)[2] = data_ptr[t + 2];
                    }
                    else
                    {
                        (data_ptr + x * nChannels + y * mipl.widthStep)[0] = 0;
                        (data_ptr + x * nChannels + y * mipl.widthStep)[1] = 0;
                        (data_ptr + x * nChannels + y * mipl.widthStep)[2] = 0;
                    }
                }
            }

        }
    }
}
